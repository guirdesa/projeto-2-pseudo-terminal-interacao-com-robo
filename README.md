# Projeto 2  - Pseudo Terminal Interação com Robô

Programa para utilização de pseudo terminal para controle de um robô.
Para utilizar o programa, primeiramente, deve-se iniciar o terminal e digitar o comando que deseja executar, após isso, o programa apresentará uma tabela relacionando o comando digitado com os pré-definidos e a % de semelhança entre eles.
Posteriormente, o usuário terá a opção de confirmar ou não a ação desejada, o usuário deve responder com "sim" se o comando for o correto, 
qualquer outra resposta será interpretada como errada.
Caso o comando não recomende o comando desejado, o programa indicará as outras opções e o mesmo procedimento de pergunta e resposta repetirá.
Após o programa reconhecer o comando desejado, ele vai mostrá-lo juntamente com seu codigo hexadecimal. No final do processo, o programa registrará os comandos selecionados e depois mostrará quantas vezes foram escolhidos.

link do video
https://youtu.be/OSmg_-B6LXc

Conteúdo do projeto está na branch "codigo"

